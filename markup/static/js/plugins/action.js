$(function() {
    function showModal(params) {
        var modal         = $('#modal'),
            modal_dialog  = modal.find('.modal-dialog'),
            modal_content = modal.find('.modal-content'),
            modal_body, modal_close;

        modal_dialog.removeClass() .addClass('modal-dialog');
        modal_content.empty();

        modal_body  = $('<div class="modal-body"><span class="loader"></span></div>').appendTo(modal_content),
        modal_close = $('<a href="javascript:void(0)" onclick="return false;" class="modal-close" data-dismiss="modal"></a>').appendTo(modal_content);

        if(params && params.className) modal_dialog.addClass(params.className);

        modal.modal({
            show: true,
            keyboard: false,
            backdrop: 'static'
        });

        if(params && params.url) {
            $.get(params.url+((params.url.indexOf('?')+1) ? '&' : '?')+'isNaked=1', function(data){
                modal_body.html(data);
                // mask_fields();
                // modal_body.find('.selectpicker').selectpicker();
            });
        }

        if(params && params.html) {
            modal_body.html(params.html);
            // mask_fields();
            // modal_body.find('.selectpicker').selectpicker();
        }

        modal.on('hidden.bs.modal', function() {
            $(this).removeData('title');
            $(this).removeData('bs.modal');

            modal_content.empty();

            if(params && params.className) modal_dialog.removeClass(params.className);

            if(params && params.location) {
                switch(params.location) {
                    case 'reload':
                        window.location = window.location;

                        break;
                    default:
                        window.location = params.location;

                        break;
                }
            }
        });
    }

    function actions($this) {
        var actions = $this.data('action') || $this.data('onchange-action');

        $.each((typeof actions!='object' ? [actions] : actions), function(action, params) {
            switch(typeof action!='string' ? params : action) {
                case 'change-tab':
                    var block = $this.parents('[data-type="tabs"]'),
                        tab_contents = block.find('[data-type="tab-content"]'),
                        tabs = block.find('[data-type="tab"]');

                    tab_contents.removeClass('active');
                    tab_contents.filter('[data-tab-content="'+params.tab+'"]').addClass('active');

                    tabs.removeClass('active');
                    tabs.filter('[data-tab="'+params.tab+'"]').addClass('active');

                    break;
                case 'submit-form':
                    var form = $this.parents('form'),
                        result = form.find('.result');

                    if(!form.find('input[name="isNaked"]').length) form.prepend('<input type="hidden" name="isNaked" value="1">');
                    if(!form.find('input[name="posting"]').length) form.prepend('<input type="hidden" name="posting" value="1">');

                    $this.attr('disabled', 'disabled');

                    if($this.text()) $this.data('submit-txt', $this.text());
                    if($this.val()) $this.data('submit-val', $this.val());

                    form.ajaxSubmit(function(data){
                        data = $.parseJSON(data);

                        if(result) {
                            result.empty();

                            if(data.text) {
                                result = $('<span class="msg msg_'+(data.type=='error' ? 'error' : 'success')+'"/>').appendTo(result);
                                result.append(data.text);
                            }
                        }

                        if(data.type=='success') {
                            if(data.modal) {
                                showModal({html: data.text, className: 'message', location: data.location});
                            } else {
                                if(data.location) {
                                    if($this.parents('#modal').length) {
                                        $('#modal').on('hidden.bs.modal', function() {
                                            window.location = data.location;
                                        });
                                    } else {
                                        if(data.location=='reload') {
                                            data.location = window.location;
                                        } else {
                                            window.location = data.location;
                                        }
                                    }
                                }
                            }

                            if(!data.not_clear_form) form.resetForm();
                        }

                        if(data.type=='error') {
                            if(data.fields && data.fields.length) {
                                var error_fields = [];

                                $.each(data.fields, function(i, val) {
                                    var field = form.find('[name="'+val+'"]');

                                    error_fields.push(field);

                                    if(field.data('error-target')) {
                                        field.parents(field.data('error-target')).addClass(field.data('error-class') || 'error');
                                    } else {
                                        field.addClass(field.data('error-class') || 'error');
                                    }

                                    field.one('keydown', function() {
                                        if(field.data('error-target')) {
                                            field.parents(field.data('error-target')).removeClass(field.data('error-class') || 'error');
                                        } else {
                                            field.removeClass(field.data('error-class') || 'error');
                                        }
                                    });
                                });

                                error_fields[0].focus();
                            }
                        }

                        $this.removeAttr('disabled');

                        if($this.data('submit-txt')) $this.text($this.data('submit-txt'));
                        if($this.data('submit-val')) $this.val($this.data('submit-val'));
                    });

                    break;
                case 'show-modal':
                    showModal(params);
                    break;
            }
        });
    }

    $('body').prepend('<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"></div></div></div>');

    $('body').on('click', '[data-action]', function(event) {
        event.preventDefault();

        actions($(this));
    });

    $('body').on('change', '[data-onchange-action]', function() {
        actions($(this));
    });
});